# About

tcn-service is a Networking Layer over the BE provided for the FIFA project. It is curnently mapping over the FIFA.ClientApp.Registration.Api v1.0.
The Swagger can be previewd [here](http://20.82.216.130/index.html).
> _*insert a more detailed award winning description here*_

## Prerequisites

The app is written in TypeScript. We assume that you already got a working Node.js system. If not, please check out the official documentation written by ©OpenJS Foundation in order to get things working fast. [You can find the docs here.](https://nodejs.org/en/docs/) 

You will also need Yarn installed. [You can find the docs here.](https://yarnpkg.com/getting-started)

### Versions

We use some npm packages with specific versions and _**we will keep the table below updated**_ in order to maintain consistency between developers and technologies.

---

#### *dependencies*

| Package    | Version |
| ---------- | ------- |
| axios      | ^0.21.1 |
| typescript | ^4.3.5  |
| @types/node | ^16.7.2 |
# 1. Getting the files

## BitBucket

We assume that you already have access to the `tcn-service` project on BitBucket and also you configured your account by adding a SSH key in order to be able to perform `git` operations on the remote git server. If not, [there is an official document teaching you how to do it.](https://bitbucket.org/product/guides)

## Fire up a new terminal window and use the following commands:

- The command below will create a new `code` directory into the `home` of the current user. **Feel free not to do it if you already have a place to store code**. Once the directory is created, we will `cd` into it.
  
```zsh
mkdir ~/code && cd ~/code
```

- The command below will clone the `tcn-service` project into the current directory if we set up correctly the SSH keys.
  
```zsh
git clone git@bitbucket.org:horeacrisan/tecnet.git
```

- Change the current directory to the git project you just cloned.
  
```zsh
cd tecnet
```

- Install packages to have the functional package downloaded via Git
  
```zsh
yarn 
```

# 2. Installing module to your Project

- First of all, you need to be connected to the feed. In order to do that, you'll need access to the ClientApp TEC Equator.
- Go to the Azure DevOps/ Artefacts/, search for ```tcn-service``` npm package, then go to ```Connect to feed```. You'll need to choose npm from the left bar, then follow the steps under the ```Project setup```  depending on your operating system.
- After the Project is set up with the ```.npmrc``` file, make sure you are in the project root directory and run the following command in order to get node modules installed.
  
```zsh
yarn add tcn-service
```
or
```zsh
npm install tcn-service
```

- Now you'll need to see ```tcn-service``` in your ```package.json``` file. Keep in mind to always install the latest version of the package.
  
# 3. App Structure

The main project root contains the `node_modules` and `src` directories at the top level, beside `package-lock.json` and `package.json` files required to manage dependencies and provide an entry point for the networking layer.

```zsh
tcn-service
|-- src/
|-- node_modules/
|-- build/
|-- package-lock.json
|-- package.json
```

## Main Directories

- `node_modules` directory contains the packages needed as dependencies to run the project (such as Axios and TypeScript)
  
- `src` directory contain the necessary code in order to make the Networking layer work. It contains a generic class named GenOps and specific classes that map each entity from each endpoint.
  
## Main Files

- `package.json` contains the dependency versions and the scripts we use.
  
- `index.ts` defines the entry/exit point of the app. It is located in the `src` directory. We just import all the internal logic there and export it as a generic object containing instances to all the classes that exist within the Networking Layer.
  
## **The `src/` directory**

All the code we write live in this directory. Companions, Identification, StaticData and so on. Each class is refering at an entity from the BE, and it maps the CRUD operation that exist.

```zsh
src
|-- index.ts
|-- GenOps.ts
|-- ApiServce.ts
|-- Companions.ts
|-- Events.ts
|-- Identification.ts
|-- Individuals.ts
|-- Landing.ts
|-- Matches.ts
|-- RegistrationFormTypes.ts
|-- ReviewDetails.ts
|-- RoomSharing.ts
|-- StaticData.ts
|-- TravelAndAccommodation.ts
|-- UserDetails.ts
|-- Account.ts
```

| Class / file | Description |
| --------------------- | --------------------- |
| `index.ts`                 | Main entry point of the application |
| `GenOps.ts`                 | _*Generic*_ class that contains the CRUD operations, used as framework/mother class for specific classes                                                                                            |
| `ApiServce.ts`              | _*Generic*_ class that contains the CRUD operations, used for generic calls to any endpoint. The instances from this class can be used for testing the API and fetching data from any mocked source |
| `Companions.ts`             | _*Specific*_ class that maps `Companions` endpoints and all the operations available                                                                                                                |
| `Identification.ts`         | _*Specific*_ class that maps `Identification` endpoints and all the operations available                                                                                                            |
| `Individuals.ts`            | _*Specific*_ class that maps `Individuals` endpoints and all the operations available                                                                                                               |
| `Landing.ts`                | _*Specific*_ class that maps `Landing` endpoints and all the operations available                                                                                                                   |
| `Matches.ts`                | _*Specific*_ class that maps `Matches` endpoints and all the operations available                                                                                                                   |
| `RoomSharing.ts`            | _*Specific*_ class that maps `RoomSharing` endpoints and all the operations available                                                                                                               |
| `StaticData.ts`             | _*Specific*_ class that maps `StaticData` endpoints and all the operations available                                                                                                                |
| `TravelAndAccommodation.ts` | _*Specific*_ class that maps `TravelAndAccommodation` endpoints and all the operations available                                                                                                    |
| `UserDetails.ts`            | _*Specific*_ class that maps `UserDetails` endpoints and all the operations available                                                                                                               |
| `ReviewDetails.ts`          | _*Specific*_ class that maps `ReviewDetails` endpoints and all the operations available                                                                                                               |
| `RegistrationFormTypes.ts` | _*Specific*_ class that maps `RegistrationFormTypes` endpoints and all the operations available                                                                                                               |
| `Events.ts`            | _*Specific*_ class that maps `Events` endpoints and all the operations available                                                                                                               |
| `Account.ts`            | _*Specific*_ class that maps `Account` endpoints and all the operations available                                                                                                               |

# 4. Initialization and operating the package 

First thing after installing the package via `npm` or `yarn` is to import the `tcnService` where you'd like to use the API.

- `tcnService` is the main entrypoint of the Networking Layer.

```javascript
// Quick demo:

import tcnService from 'tcn-service';

async function getDynamicForms() {
    const response =  await tcnService.genericApi.get('https://demo5655985.mockable.io/formSchema_v1');
    return response.data
  }

getDynamicForms();
```

---

## Instances and methods

### `ApiService` :

```typescript
ApiService.get: (url: string, config?: AxiosRequestConfig) => Promise<any>
```
```typescript
ApiService.post: (url: string, data?: Object, config?: AxiosRequestConfig) => Promise<AxiosResponse<any>>
```
```typescript
ApiService.put: (url: string, data?: Object, config?: AxiosRequestConfig) => Promise<any>
```
```typescript
ApiService.delete: (url: string, config?: AxiosRequestConfig) => Promise<any>
```

### `Companions` :

```typescript
Companions.getCompanionLevelsForIndividualById(id?: string): Promise<any>
```
```typescript
Companions.addCompanion(payload: Object): Promise<any>
```
```typescript
Companions.changeCompanionType(payload: Object): Promise<any>
```
```typescript
Companions.getCompanionsForGuest(id?: string): Promise<any>
```
```typescript
Companions.getCompanionsForGuestShallow(id?: string): Promise<any>
```
```typescript
Companions.swapCompanionTypes(payload: Object): Promise<any>
```
```typescript
Companions.unassignCompanion(payload: Object): Promise<any>
```

### `Identification` :

```typescript
Identification.getIdentification(id: string): Promise<any>
```
```typescript
Identification.postEntity(payload: Object): Promise<any>
```

### `Individuals` :

```typescript
Individuals.getIndividuals(): Promise<any>
```
```typescript
Individuals.getIndividual(id: string): Promise<any>
```

### `Landing` :

```typescript
Landing.getLandingForIndividual(id: string): Promise<any>
```
```typescript
Landing.getTermsAndConditions(id: string): Promise<any>
```
```typescript
Landing.updateTermsAndConditions(id: string, likeToAttendEvent: boolean, acceptAccreditationTermsAndCondition: boolean, acceptTermsAndCondition: boolean, dataProtectionConsent: boolean): Promise<any>
```

### `Matches` :

```typescript
Matches.getMatchesForIndividual(id: string): Promise<any>
```
```typescript
Matches.getMatchesForIndividual(id: string): Promise<any>
```

### `RoomSharing` :

```typescript
RoomSharing.getRoomSharing(id: string): Promise<any>
```
```typescript
RoomSharing.updateRoomSharingRelations(payload: Object): Promise<any>
```
```typescript
RoomSharing.removeRoomSharingRelations(config: AxiosRequestConfig): Promise<any>
```

### `StaticData` :

```typescript
StaticData.getCountries(): Promise<any>
```
```typescript
StaticData.getAirlines(): Promise<any>
```
```typescript
StaticData.getSearchedAirlines(keyword: string): Promise<any>
```
```typescript
StaticData.getAircraftSeatTypes(): Promise<any>
```
```typescript
StaticData.getSalutations(): Promise<any>
```
```typescript
StaticData.getAirports(): Promise<any>
```
```typescript
StaticData.getSearchedAirports(keyword: string): Promise<any>
```

### `TravelAndAccommodation` :

```typescript
TravelAndAccommodation.getTravelAndAccommodation(id: string): Promise<any>
```
```typescript
TravelAndAccommodation.updateTravelAndAccommodation(payload: Object): Promise<any>
```

### `UserDetails` :

```typescript
UserDetails.getUserDetails(id: string): Promise<any>
```
```typescript
UserDetails.updateUserDetails(payload: Object): Promise<any>
```

### `ReviewDetails` :

```typescript
ReviewDetails.getReviewDetails(id: string): Promise<any>
```

### `RegistrationFormTypes` :

```typescript
RegistrationFormTypes.getRegistrationFormTypes(id: string): Promise<any>
```

### `Events` :

```typescript
Events.getEvents(): Promise<any>
```
```typescript
Events.getEvent(id: string): Promise<any>
```
```typescript
Events.getCurrentEvent(): Promise<any>
```

# 5. Updates and future imrpovements

This package is still in development process.
