import GenOps from './GenOps';
const API_CONTEXT_URL = 'api/individuals';
export class Individuals extends GenOps {

    constructor() {
        super();
    }

    async getIndividuals() {
        return await super.get(API_CONTEXT_URL)
    }

    async getIndividual(id: string) {
        return await super.get(API_CONTEXT_URL, id);
    }

}; export const individualsApi = new Individuals();
