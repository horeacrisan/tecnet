import GenOps from './GenOps';
const API_CONTEXT_URL = 'api/data';
export class StaticData extends GenOps {

    constructor() {
        super();
    }

    async getCountries() {
        return await super.get(`${API_CONTEXT_URL}/countries`)
    }

    async getAirlines() {
        return await super.get(`${API_CONTEXT_URL}/airlines`)
    }

    async getSearchedAirlines(keyword: string) {
        return await super.get(`${API_CONTEXT_URL}/search-airlines?SearchText=${keyword}`)
    }

    async getAircraftSeatTypes() {
        return await super.get(`${API_CONTEXT_URL}/aircraft-seat-types`)
    }

    async getSalutations() {
        return await super.get(`${API_CONTEXT_URL}/salutatuions`)
    }

    async getAirports() {
        return await super.get(`${API_CONTEXT_URL}/airports`)
    }

    async getSearchedAirports(keyword: string) {
        return await super.get(`${API_CONTEXT_URL}/search-airlines?SearchText=${keyword}`)
    }

}; export const staticDataApi = new StaticData();
