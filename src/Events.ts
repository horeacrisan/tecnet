import GenOps from './GenOps';
const API_CONTEXT_URL = 'api/form/events';
export class Events extends GenOps {

    constructor() {
        super();
    }

    async getEvents() {
        return await super.get(API_CONTEXT_URL)
    }

    async getEvent(id: string) {
        return await super.get(API_CONTEXT_URL, id);
    }

    async getCurrentEvent() {
        return await super.get(`current`);
    }

}; export const eventsApi = new Events();
