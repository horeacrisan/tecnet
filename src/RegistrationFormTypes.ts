import GenOps from './GenOps';
const API_CONTEXT_URL = '/api/form/registration-form-types';
export class RegistrationFormTypes extends GenOps {

    constructor() {
        super();
    }

    async getRegistrationFormTypes(id: string) {
        return await super.get(API_CONTEXT_URL, id);
    }
    
}; export const registrationFormTypesApi = new RegistrationFormTypes();
