import { apiService } from "./ApiService";
import { companionsApi } from './Companions';
import { identificationApi } from './Identification';
import { individualsApi } from './Individuals';
import { landingApi } from './Landing';
import { matchesApi } from './Matches';
import { roomSharingApi } from './RoomSharing';
import { staticDataApi } from './StaticData';
import { travelAndAccommodationApi } from './TravelAndAccommodation';
import { userDerailsApi } from './UserDetails';
import { reviewDetailsApi } from './ReviewDetails';
import { registrationFormTypesApi } from './RegistrationFormTypes';
import { eventsApi } from './Events';
import { accountApi } from './Account';

const tcnService = {
    companionsApi: companionsApi,
    identificationApi: identificationApi,
    individualsApi: individualsApi,
    landingApi: landingApi,
    matchesApi: matchesApi,
    roomSharingApi: roomSharingApi,
    staticDataApi: staticDataApi,
    travelAndAccommodationApi: travelAndAccommodationApi,
    userDerailsApi: userDerailsApi,
    genericApi: apiService,
    reviewDetailsApi: reviewDetailsApi,
    registrationFormTypesApi: registrationFormTypesApi,
    eventsApi: eventsApi,
    accountApi: accountApi,
};

export default tcnService;