import { AxiosInstance, AxiosRequestConfig, default as axios } from "axios";
const API_BASE_URL = "http://20.82.216.130";
export default class GenOps {

    service: AxiosInstance

    constructor() {
        this.service = axios.create({
            baseURL: API_BASE_URL,
            headers: {
                'Content-Type': 'application/json',
            },
        });
    }

    async get(url: string, uuid?: string) {
        try {
            const response = uuid ? await this.service.get(`${url}/${uuid}`) : await this.service.get(url);
            return response;
        } catch (error) {
            return error;
        }
    }

    async post(url: string, payload?: Object, uuid?: string) {
        try {
            const response = await this.service.post(`${url}/${uuid}`, payload);
            return response;
        } catch (error) {
            return error;
        }
    }

    async delete(url: string, config?: AxiosRequestConfig) {
        try {
            const response = config ? await this.service.delete(url, config) : await this.service.delete(url);
            return response;
        } catch (error) {
            return error;
        }
    }
};
