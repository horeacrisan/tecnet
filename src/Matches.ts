import GenOps from './GenOps';
const API_CONTEXT_URL = '/api/form/matches';
export class Matches extends GenOps {

    constructor() {
        super();
    }

    async getMatchesForIndividual(id: string) {
        return await super.get(API_CONTEXT_URL, id);
    }

    async updateMatchesForIndividual(id: string, payload: Object) {
        return await super.post(API_CONTEXT_URL, payload, id);
    }

}; export const matchesApi = new Matches();
