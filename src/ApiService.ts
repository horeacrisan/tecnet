import axios, { AxiosInstance, AxiosRequestConfig } from "axios";
export class ApiService {
    service: AxiosInstance

    constructor() {
        this.service = axios.create({
            headers: {
                'Content-Type': 'application/json',
            }
        });
    }

    get = async (url: string, config?: AxiosRequestConfig) => {
        try {
            const response = await this.service.get(url, config);
            return response;
        } catch (error) {
            return error
        }
    };

    post = async (url: string, data?: Object, config?: AxiosRequestConfig) => {
        try {
            const response = data ? await this.service.post(url, data, config) : await this.service.post(url, null, config);
            return response;
        } catch (error) {
            return error;
        }
    }

    put = async (url: string, data?: Object, config?: AxiosRequestConfig) => {
        try {
            const response = await this.service.put(url, data, config);
            return response;
        } catch (error) {
            return error;
        }
    }

    delete = async (url: string, config?: AxiosRequestConfig) => {
        try {
            const response = await this.service.delete(url, config);
            return response;
        } catch (error) {
            return error;
        }
    }

}; export const apiService = new ApiService();
