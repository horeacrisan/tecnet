import GenOps from './GenOps';
const API_CONTEXT_URL = '/api/form/companions';
export class Companions extends GenOps {

    constructor() {
        super();
    }

    async getCompanionLevelsForIndividualById(id?: string) {
        return await super.get(`${API_CONTEXT_URL}/get-companion-levels-for-individual-by-id?id=${id}`);
    }

    async addCompanion(payload: Object) {
        return await super.post(`${API_CONTEXT_URL}/add-companion`, payload);
    }

    async changeCompanionType(payload: Object) {
        return await super.post(`${API_CONTEXT_URL}/change-companion-type`, payload);
    }

    async getCompanionsForGuest(id?: string) {
        return await super.get(`${API_CONTEXT_URL}/get-companions-for-guest?id=${id}`);
    }

    async getCompanionsForGuestShallow(id?: string) {
        return await super.get(`${API_CONTEXT_URL}/get-companions-for-guest-shallow?id=${id}`)
    }

    async swapCompanionTypes(payload: Object) {
        return await super.post(`${API_CONTEXT_URL}/swap-companion-types`, payload);
    }

    async unassignCompanion(payload: Object) {
        return await super.post(`${API_CONTEXT_URL}/unassign-companion`, payload);
    }

}; export const companionsApi = new Companions();
