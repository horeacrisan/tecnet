import GenOps from './GenOps';
const API_CONTEXT_URL = '/api/form/review-details';
export class ReviewDetails extends GenOps {

    constructor() {
        super();
    }

    async getReviewDetails(id: string) {
        return await super.get(API_CONTEXT_URL, id);
    }
    
}; export const reviewDetailsApi = new ReviewDetails();
