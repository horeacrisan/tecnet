import { AxiosRequestConfig } from 'axios';
import GenOps from './GenOps';
const API_CONTEXT_URL = '/api/form/room-sharing';
export class RoomSharing extends GenOps {

    constructor() {
        super();
    }

    async getRoomSharing(id: string) {
        return await super.get(`${API_CONTEXT_URL}?participantId=${id}`);
    }

    async updateRoomSharingRelations(payload: Object) {
        return await super.post(`${API_CONTEXT_URL}/add-room-sharing-relations`, payload);
    }

    async removeRoomSharingRelations(config: AxiosRequestConfig) {
        return await super.delete(`${API_CONTEXT_URL}/remove-room-sharing-relations`, config);
    }
    
}; export const roomSharingApi = new RoomSharing();
