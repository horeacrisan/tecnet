import GenOps from './GenOps';
const API_CONTEXT_URL = '/api/form/travel-and-accomodation';
export class TravelAndAccommodation extends GenOps {

    constructor() {
        super();
    }

    async getTravelAndAccommodation(id: string) {
        return await super.get(API_CONTEXT_URL, id);
    }

    async updateTravelAndAccommodation(payload: Object,) {
        return await super.post(API_CONTEXT_URL, payload);
    }

}; export const travelAndAccommodationApi = new TravelAndAccommodation();
