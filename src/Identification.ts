import GenOps from './GenOps';
const API_CONTEXT_URL = '/api/form/identification';
export class Identification extends GenOps {

    constructor() {
        super();
    }

    async getIdentification(id: string) {
        return await super.get(API_CONTEXT_URL, id);
    }

    async postEntity(payload: Object) {
        return await super.post(API_CONTEXT_URL, payload);
    }
}; export const identificationApi = new Identification();
