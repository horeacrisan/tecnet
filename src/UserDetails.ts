import GenOps from './GenOps';
const API_CONTEXT_URL = '/api/form/user-details';
export class UserDetails extends GenOps {

    constructor() {
        super();
    }

    async getUserDetails(id: string) {
        return await super.get(API_CONTEXT_URL, id);
    }

    async updateUserDetails(payload: Object,) {
        return await super.post(API_CONTEXT_URL, payload);
    }

}; export const userDerailsApi = new UserDetails();
