import axios, { AxiosInstance } from "axios";

const API_BASE_URL = 'http://52.236.147.195/';

export class AccountService {
    service: AxiosInstance;
    
    constructor() {
        this.service = axios.create({
            baseURL: API_BASE_URL,
            headers: {
                'Content-Type': 'application/json',
            },
        });
    }

    validateEmailAddress(email: string) {
        return this.service.get(`/api/myaccount/accounts/validate/${email}`);
    }
}

export const accountApi = new AccountService();