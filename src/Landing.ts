import GenOps from './GenOps';
const API_CONTEXT_URL = '/api/form/landing';
export class Landing extends GenOps {

    constructor() {
        super();
    }

    async getLandingForIndividual(id: string) {
        return await super.get(API_CONTEXT_URL, id);
    }

    async getTermsAndConditions(id: string) {
        return await super.get(`${API_CONTEXT_URL}/get-terms-and-condition?id=${id}`);
    }

    async updateTermsAndConditions(
            id: string,
            likeToAttendEvent: boolean,
            acceptAccreditationTermsAndCondition: boolean,
            acceptTermsAndCondition: boolean,
            dataProtectionConsent: boolean) {
        return await super.post(`${API_CONTEXT_URL}/update-terms-and-conditions
            ?Id=${id}
            ?LikeToAttendEvent=${likeToAttendEvent}
            ?AcceptAccreditationTermsAndCondition=${acceptAccreditationTermsAndCondition}
            ?AcceptTermsAndCondition=${acceptTermsAndCondition}
            ?DataProtectionConsent=${dataProtectionConsent}`);
    }

}; export const landingApi = new Landing();
